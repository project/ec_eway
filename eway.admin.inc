<?php
/**
 * @file
 * EWAY Administration functions.
 */

/**
 * eWAY Settings Page
 */
function ec_eway_hosted_settings($form, &$form_state) {
  $form = array();

  $form['ec_eway_clientid'] = array(
    '#type' => 'textfield',
    '#title' => t('eWAY Client Id'),
    '#default_value' => variable_get('ec_eway_clientid', '87654321'),
    '#size' => 70,
    '#maxlength' => 70,
    '#description' => t("Client Id that was issued by eWAY"),
    '#required' => TRUE,
  );
  $form['ec_eway_username'] = array(
    '#type' => 'textfield',
    '#title' => t('eWAY Username'),
    '#default_value' => variable_get('ec_eway_username', 'test@eway.com.au'),
    '#size' => 70,
    '#maxlength' => 70,
    '#description' => t("Username that was issued by eWAY"),
    '#required' => TRUE,
  );
  $form['ec_eway_password'] = array(
    '#type' => 'textfield',
    '#title' => t('eWAY Password'),
    '#default_value' => variable_get('ec_eway_password', 'test123'),
    '#size' => 70,
    '#maxlength' => 70,
    '#description' => t("Password that was issued by eWAY"),
    '#required' => TRUE,
  );
  $form['ec_eway_default_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Invoice Description'),
    '#default_value' => variable_get('ec_eway_default_description', ''),
    '#size' => 70,
    '#maxlength' => 70,
    '#description' => t("The default description that appears on an invoice"),
    '#required' => TRUE,
  );
  $form['ec_eway_debug'] = array(
    '#type' => 'radios',
    '#title' => t('eWAY test mode'),
    '#default_value' => variable_get('ec_eway_debug', 1),
    '#options' => array(t('Disabled'), t('Enabled')),
    '#description' => t('If enabled, transactions will be sent in test mode and cards will not be charged.'),
  );

  return system_settings_form($form);
}

/**
 * eWAY Settings Page
 */
function ec_eway_shared_settings($form, &$form_state) {
  $form = array();

  $form['ec_eway_clientid'] = array(
    '#type' => 'textfield',
    '#title' => t('eWAY Client Id'),
    '#default_value' => variable_get('ec_eway_clientid', '87654321'),
    '#size' => 70,
    '#maxlength' => 70,
    '#description' => t("Client Id that was issued by eWAY"),
    '#required' => TRUE,
  );
  $form['ec_eway_username'] = array(
    '#type' => 'textfield',
    '#title' => t('eWAY Username'),
    '#default_value' => variable_get('ec_eway_username', 'TestAccount'),
    '#size' => 70,
    '#maxlength' => 70,
    '#description' => t("Username that was issued by eWAY (could be email address you access eWay with)"),
    '#required' => TRUE,
  );
  $form['ec_eway_default_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Invoice Description'),
    '#default_value' => variable_get('ec_eway_default_description', ''),
    '#size' => 70,
    '#maxlength' => 70,
    '#description' => t("The default description that appears on an invoice"),
    '#required' => TRUE,
  );
  $form['ec_eway_debug'] = array(
    '#type' => 'radios',
    '#title' => t('eWAY test mode'),
    '#default_value' => variable_get('ec_eway_debug', 1),
    '#options' => array(t('Disabled'), t('Enabled')),
    '#description' => t('If enabled, transactions will be sent in test mode and cards will not be charged.'),
  );

  return system_settings_form($form);
}
