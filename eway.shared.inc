<?php
/**
 * @file
 * Implementation of EWAY's shared payment gateway.
 */

/**
 * Implements hook_receipt_process_payment().
 */
function ec_eway_shared_receipt_process_payment($receipt, $atype, $object) {
  $request = array(
    'CustomerID' => variable_get('ec_eway_clientid', '87654321'),
    'UserName' => variable_get('ec_eway_username', 'TestAccount'),
    'Currency' => $receipt->currency,
    'ReturnURL' => url('eway/return', array('absolute' => TRUE)),
    'CancelURL' => url('eway/cancel', array('query' => array('receipt' => ''), 'absolute' => TRUE)),
    'CustomerDescription' => variable_get('ec_eway_default_description', ''),
    'MerchantReference' => $receipt->erid,
    'MerchantOption1' => 'erid=' . $receipt->erid,
  );

  if ($total = ec_receipt_alloc_invoke($atype, 'get_total', $object)) {
    //Accepts Total in Cents
    $request['Amount'] = number_format($total, 2, '.', '');
  }

  if ($name = ec_receipt_alloc_invoke($atype, 'get_customer_names', $object)) {
    $request['CustomerFirstName'] = $name['fname'];
    $request['CustomerLastName'] = $name['lname'];
  }
  if ($address = ec_receipt_alloc_invoke($atype, 'get_address', $object, 'billing')) {
    $request['CustomerAddress'] = $address['street1'] . (isset($address['street2']) ? ', ' . $address['street2'] : '');
    $request['CustomerCity'] = $address['city'];
    $request['CustomerState'] = $address['state'];
    $request['CustomerPostcode'] = $address['zip'];
    $request['CustomerCountry'] = $address['country'];
  }

  $uri = url('https://au.ewaygateway.com/Request', array('query' => $request));

  $headers = array(
    'Content-Type' => 'application/x-www-form-urlencoded',
  );
  $ret = drupal_http_request($uri, array('headers' => $headers));

  $response = simplexml_load_string($ret->data);

  switch ($response->Result) {
    case 'True':
      $data = array(
        'url' => (string) $response->URI,
      );
      ec_receipt_alloc_invoke($atype, 'set_payment_data', $object, $data);
      break;

    case 'False':
      form_set_error('', (string) $response->Error);
      break;
  }
  return $receipt->erid;
}

/**
 * Implements hook_receipt_payment_url().
 */
function ec_eway_shared_receipt_payment_url($receipt, $atype, $object) {
  $data = ec_receipt_alloc_invoke($atype, 'get_payment_data', $object);
  return $data['eway_shared']['url'];
}

/**
 * Process returning receipts.
 */
function ec_eway_shared_return() {
  if (isset($_POST['AccessPaymentCode'])) {
    $request = array(
      'CustomerID' => variable_get('ec_eway_clientid', '87654321'),
      'UserName' => variable_get('ec_eway_username', 'TestAccount'),
      'AccessPaymentCode' => $_POST['AccessPaymentCode'],
    );

    $uri = url('https://au.ewaygateway.com/Result', array('query' => $request));

    $headers = array(
      'Content-Type' => 'application/x-www-form-urlencoded',
    );
    $ret = drupal_http_request($uri, array('headers' => $headers));

    $response = simplexml_load_string($ret->data);

    $receipt = ec_receipt_load($response->MerchantReference);

    switch ($response->ResponseCode) {
      case '00':
      case '08':
      case '10':
      case '11':
      case '16':
        $receipt->status = RECEIPT_STATUS_COMPLETED;
        $receipt->amount = (string) $response->ReturnAmount;
        $receipt->extern_id = (string) $response->AuthCode;
        break;

      default:
        $receipt->status = RECEIPT_STATUS_FAILED;
        $receipt->response_text = (string) $response->ErrorMessage;
    }

    ec_receipt_save($receipt);

    $return_url = ec_receipt_return_page(isset($receipt) ? $receipt : NULL);
    if (is_array($return_url)) {
      call_user_func_array('drupal_goto', $return_url);
    }
    else {
      drupal_goto($return_url);
    }
  }
  drupal_access_denied();
}

/**
 * Return cancelled receipts
 */
function ec_eway_shared_cancel() {
  if (isset($_REQUEST['receipt']) && ($receipt = ec_receipt_load($_REQUEST['receipt']))) {
    $receipt->status = RECEIPT_STATUS_CANCELLED;

    drupal_set_message(t('Receipt %erid has been canceled.', array('%erid' => $receipt->erid)));

    ec_receipt_save($receipt);
  }

  $cancel_url = ec_receipt_cancel_page(isset($receipt) ? $receipt : NULL, isset($_REQUEST['invoice']) ? $_REQUEST['invoice'] : NULL);
  if (is_array($cancel_url)) {
    call_user_func_array('drupal_goto', $cancel_url);
  }
  else {
    drupal_goto($cancel_url);
  }
}
