<?php
/**
 * @file
 * Implementation of EWAY Hosted Payment Gateway.
 */

/**
 * Implements hook_receipt_payment_form().
 */
function ec_eway_hosted_receipt_payment_form($type, $object) {
  $form = array();

  $cc = ec_receipt_alloc_invoke($type, 'get_payment_data', $object);
  $form['cc'] = array(
    '#type' => 'credit_card',
    '#title' => t('Credit card details'),
    '#cvnrequired' => TRUE,
    '#default_value' => isset($cc['cc']) ? $cc['cc'] : NULL,
  );
  return $form;
}

/**
 * Implements hook_receipt_process_payment().
 */
function ec_eway_hosted_receipt_process_payment($receipt, $atype, $object) {
  if ($recurring = ec_receipt_alloc_invoke($atype, 'get_recurring', $object)) {
    return ec_eway_hosted_receipt_process_payment_rebill($receipt, $atype, $object, $recurring);
  }
  else {
    return ec_eway_hosted_receipt_process_payment_hosted($receipt, $atype, $object);
  }
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function ec_eway_hosted_receipt_process_payment_hosted($receipt, $atype, $object) {
  // set the correct URL
  if (variable_get('ec_eway_debug', 1)) {
    $gateway_url = 'https://www.eway.com.au/gateway/xmltest/TestPage.asp';
  }
  else {
    $gateway_url = 'https://www.eway.com.au/gateway/xmlpayment.asp';
  }

  $request = array(
    'ewayCustomerID' => variable_get('ec_eway_clientid', '87654321'),
    'ewayCustomerInvoiceDescription' => variable_get('ec_eway_default_description', ''),
    'ewayCustomerEmail' => '',
    'ewayCustomerInvoiceRef' => '',
    'ewayTrxnNumber' => '',
    'ewayOption1' => '',
    'ewayOption2' => '',
    'ewayOption3' => '',
  );
  if ($total = ec_receipt_alloc_invoke($atype, 'get_total', $object)) {
    //Accepts Total in Cents
    $request['ewayTotalAmount'] = number_format($total, 2, '', '');
  }
  if ($cc = ec_receipt_alloc_invoke($atype, 'get_payment_data', $object)) {
    $request['ewayCardHoldersName'] = $cc['cc']['name'];
    $request['ewayCardNumber'] = $cc['cc']['cardnumber'];
    $request['ewayCardExpiryMonth'] = $cc['cc']['expiry']['expmonth'];
    $request['ewayCardExpiryYear'] = $cc['cc']['expiry']['expyear'];
    if (!empty($cc['cc']['cvn'])) {
      $request['ewayCVN'] = $cc['cc']['cvn'];
    }
    // set the correct URL
    if (variable_get('ec_eway_debug', 1)) {
      $gateway_url = 'https://www.eway.com.au/gateway_cvn/xmltest/testpage.asp';
    }
    else {
      $gateway_url = 'https://www.eway.com.au/gateway_cvn/xmlpayment.asp';
    }
  }

  if ($name = ec_receipt_alloc_invoke($atype, 'get_customer_names', $object)) {
    $request['ewayCustomerFirstName'] = $name['fname'];
    $request['ewayCustomerLastName'] = $name['lname'];
  }
  if ($address = ec_receipt_alloc_invoke($atype, 'get_address', $object, 'billing')) {
    $request['ewayCustomerAddress'] = $address['street1'] . ', ' . $address['street2'] .
      " \n" . $address['city'] . ', ' . $address['state'];
    $request['ewayCustomerPostcode'] = $address['zip'];
  }

  $headers = array(
    'Content-Type' => 'application/x-www-form-urlencoded',
  );

  $request_xml = '<ewaygateway>';
  foreach ($request as $key => $value) {
    $request_xml .= "<$key>$value</$key>";
  }
  $request_xml .= '</ewaygateway>';

  $ret = drupal_http_request(variable_get('ec_eway_url', $gateway_url), array('headers' => $headers, 'method' => 'POST', 'data' => $request_xml));

  $response = simplexml_load_string($ret->data);

  switch ($response->ewayTrxnStatus) {
    case 'True':
      $receipt->status = RECEIPT_STATUS_COMPLETED;
      $receipt->approval_code = (string) $response->ewayAuthCode;
      $receipt->extern_id = (string) $response->ewayTrxnNumber;
      break;

    case 'False':
      $receipt->status = RECEIPT_STATUS_FAILED;
      $receipt->response_text = (string) $response->ewayTrxnError;
      form_set_error('', $receipt->response_text);
      break;
  }
  ec_receipt_save($receipt);

  return $receipt->erid;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function ec_eway_hosted_receipt_process_payment_rebill($receipt, $atype, $object, $recurring) {
  // set the correct URL
  if (variable_get('ec_eway_debug', 1)) {
    $gateway_url = 'https://www.eway.com.au/gateway/rebill/test/manageRebill_test.asmx';
  }
  else {
    $gateway_url = 'https://www.eway.com.au/gateway/rebill/manageRebill.asmx';
  }

  $client = &ec_eway_hosted_get_soap_connection($gateway_url);

  $customer = ec_receipt_alloc_invoke($atype, 'get_customer', $object);

  /**
   * If there is no eWay Rebill customer id then we need to create one.
   * Note: If there is a customer already should we update the customer.
   */
  if (!isset($customer->eway_rebill_customer_id) || !$customer->eway_rebill_customer_id) {
    $new_customer = (object) array(
      'customerRef' => '',
      'customerTitle' => '',
      'customerFirstName' => '',
      'customerLastName' => '',
      'customerCompany' => '',
      'customerJobDesc' => '',
      'customerEmail' => '',
      'customerAddress' => '',
      'customerSuburb' => '',
      'customerState' => '',
      'customerPostCode' => '',
      'customerCountry' => '',
      'customerPhone1' => '',
      'customerPhone2' => '',
      'customerFax' => '',
      'customerURL' => '',
      'customerComments' => '',
    );

    $new_customer->customerRef = $customer->ecid;
    if ($names = ec_receipt_alloc_invoke($atype, 'get_customer_names', $object)) {
      $new_customer->customerFirstName = $names['fname'];
      $new_customer->customerLastName = $names['lname'];
      if ($mail = ec_receipt_alloc_invoke($atype, 'get_mail', $object)) {
        $new_customer->customerEmail = $mail;
      }
    }
    else {
      form_set_error('', t('Unable to retrive customer name'));
      return;
    }

    $result = $client->CreateRebillCustomer($new_customer);
    if ($result->CreateRebillCustomerResult->Result == 'Success') {
      $customer->eway_rebill_customer_id = $result->CreateRebillCustomerResult->RebillCustomerID;
      ec_customer_update($customer);
    }
    else {
      form_set_error('', $result->CreateRebillCustomerResult->ErrorDetails);
      return;
    }
  }

  $event = (object) array(
    'RebillCustomerID' => $customer->eway_rebill_customer_id,
    'RebillInvRef' => $receipt->erid,
    'RebillInvDes' => variable_get('ec_eway_default_description', t('@site_name purchase', array('@site_name' => variable_get('site_name', t('Drupal'))))),
    'RebillCCName' => '',
    'RebillCCNumber' => '',
    'RebillCCExpMonth' => '',
    'RebillCCExpYear' => '',
    'RebillInitAmt' => '',
    'RebillInitDate' => '',
    'RebillRecurAmt' => '',
    'RebillStartDate' => '',
    'RebillInterval' => '',
    'RebillIntervalType' => '',
  );

  if ($cc = ec_receipt_alloc_invoke($atype, 'get_payment_data', $object)) {
    $event->RebillCCName = $cc['cc']['name'];
    $event->RebillCCNumber = $cc['cc']['cardnumber'];
    $event->RebillCCExpMonth = $cc['cc']['expiry']['expmonth'];
    $event->RebillCCExpYear = $cc['cc']['expiry']['expyear'];
    $event->RebillInitAmt = number_format(ec_receipt_alloc_invoke($atype, 'get_total', $object) * 100, 0);
    $event->RebillInitDate = format_date(REQUEST_TIME, 'custom', 'd/m/Y');
    $event->RebillRecurAmt = number_format($recurring['price'] * 100, 0);
    $event->RebillStartDate = format_date($recurring['start_date'], 'custom', 'd/m/Y');
    $event->RebillInterval = $recurring['period'];
    $event->RebillIntervalType = ec_eway_hosted_get_interval_type($recurring['unit']);

    if ($enddate = ec_receipt_alloc_invoke($atype, 'get_recurring_enddate', $object, $recurring)) {
      $event->RebillEndDate = format_date($enddate, 'custom', 'd/m/Y');
    }

    $result = $client->CreateRebillEvent($event);
    if ($result->CreateRebillEventResult->Result == 'Success') {
      $receipt->status = RECEIPT_STATUS_COMPLETED;
      $receipt->eway_rebill_id = $result->CreateRebillEventResult->RebillID;
    }
    else {
      $receipt->status = RECEIPT_STATUS_FAILED;
      $receipt->response_text = $result->CreateRebillEventResult->ErrorDetails;
      form_set_error('', $receipt->response_text);
    }
  }

  ec_receipt_save($receipt);

  return $receipt->erid;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function ec_eway_hosted_get_interval_type($interval) {
  switch ($interval) {
    case 'D':
      return 1;

    case 'W':
      return 2;

    case 'M':
      return 3;

    case 'Y':
      return 4;
  }

  return 0;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function ec_eway_hosted_get_soap_connection($uri) {
  static $client;

  if (!isset($client)) {
    $client = new SoapClient($uri . '?WSDL', array('trace' => 1));

    $header_array = array(
      'eWAYCustomerID' => variable_get('ec_eway_clientid', '87654321'),
      'Username' => variable_get('ec_eway_username', 'test@eway.com.au'),
      'Password' => variable_get('ec_eway_password', 'test123'),
    );

    $header = new SoapHeader('http://www.eway.com.au/gateway/rebill/manageRebill', 'eWAYHeader', $header_array);

    $client->__setSoapHeaders(array($header));
  }

  return $client;
}
